/** \file QPromise.cpp
  * \brief Implementation of the Promises/A+ - like QPromise class
  * \author Devyatnikov A.
  * \date 21.03.2016
  * Project: QPromise
  *
  * This is an adaptation from https://github.com/rhashimoto/poolqueue
  */

#include "QPromise.h"

#include <QTimer>
#include <QTimerEvent>
#include <QThreadPool>
#include <QThreadStorage>

#include <thread>
#include <cassert>
#include <iostream>
#include <mutex>
#include <vector>
#include <queue>

Q_LOGGING_CATEGORY(QPromiseLogCategory, "QPromise")

namespace {

  // Private type to signify an unset QPromise value. This allows void
  // to be a valid value.
  struct Unset
  {
  };

  // This is the handler called when a QPromise is destroyed and it
  // contains an undelivered exception. There is nothing technically
  // wrong with discarding an undelivered exception but it is much
  // harder to figure out certain bugs. The default handler thus
  // aborts.
  static void promise_default_unhandled_exception_handler(const std::exception_ptr& e)
  {
    try
    {
      if (e)
        std::rethrow_exception(e);
      else
        qCCritical(QPromiseLogCategory) << "leaked null failure";
    }
    catch (const std::exception& e)
    {
      qCCritical(QPromiseLogCategory) << "leaked exception" << QString::fromLocal8Bit(e.what());
    }
    catch (...)
    {
      qCCritical(QPromiseLogCategory) << "leaked unknown exception";
    }

    // This function is called from a destructor so just abort
    // instead of throwing.
    std::unexpected();
  }
  QPromise::ExceptionHandler undeliveredExceptionHandler = &promise_default_unhandled_exception_handler;

  // This is the handler called on a bad callback argument cast,
  // i.e. when the resolved value on a QPromise is incompatible with
  // the onFulfil argument. If the handler returns normally, the
  // exception will be propagated along the QPromise chain like any
  // other exception. The default handler throws because this is
  // typically a programming error.
  QPromise::BadCastHandler badCastHandler = [](const QPromise::bad_cast& e)
  {
    throw e;
  };
}

/** Thread local helper object used to delay execution of some function or
 * forward it to some thread
 */
class AsyncQueue: public QObject
{
  Q_OBJECT
  Q_DISABLE_COPY(AsyncQueue)
public:
  AsyncQueue()
    : m_isInvoking(false)
  {}

  virtual ~AsyncQueue()
  {
    if (!m_callQueue.empty() || !m_timerMap.empty())
      qCWarning(QPromiseLogCategory) << "Destroying AsyncQueue with" << (m_callQueue.size() + m_timerMap.size()) << "calls still in queue";
  }

  /** Calls the speicified function in the context of the thread this object
   * belongs to. This call made later using thread's QEventLoop
   *
   * This function is thread-safe and may be used from different threads.
   */
  void callLater(std::function<void()>&& fn)
  {
    bool needInvoke = false;
    {
      std::lock_guard<decltype(m_mutex)> lock(m_mutex);

      m_callQueue.emplace_back(std::move(fn));

      if (!m_isInvoking)
      {
          m_isInvoking = true;
          needInvoke = true;
      }
    }

    if (needInvoke)
      QMetaObject::invokeMethod(this, "processQueue", Qt::QueuedConnection);
  }

  /** Calls the specified function with a specified delay in milliseconds.
   *
   * This function is not thread-safe and should be used in the context of the
   * thread, that created this object.
   *
   * If the "delayMs" is 0, then this function immediately calls #callLater(std::function<void()>&&),
   * so it is implicitly thread safe in that particular case.
   */
  void callLater(int delayMs, std::function<void()>&& fn)
  {
    if (!delayMs)
      return callLater(std::move(fn));

    Q_ASSERT(QThread::currentThread() == thread());
    if (QThread::currentThread() != thread())
      throw std::logic_error("Invalid timer call from different thread");

    int timerId = startTimer(delayMs);
    m_timerMap.emplace(timerId, std::move(fn));
  }

private:
  Q_INVOKABLE void processQueue()
  {
    std::unique_lock<decltype(m_mutex)> lock(m_mutex);
    std::vector<std::function<void()>> callQueue = std::move(m_callQueue);
    m_isInvoking = false;
    lock.unlock();

    for (auto fn : callQueue)
    {
      try
      {
        fn();
      }
      catch (...)
      {
        undeliveredExceptionHandler(std::current_exception());
      }
    }
  }

  void timerEvent(QTimerEvent * evt)
  {
    killTimer(evt->timerId());
    auto it = m_timerMap.find(evt->timerId());
    if (it == m_timerMap.end())
      return;

    try
    {
     (it->second)();
    }
    catch (...)
    {
      undeliveredExceptionHandler(std::current_exception());
    }

    m_timerMap.erase(it);
  }

  bool m_isInvoking;
  std::mutex m_mutex;
  std::vector<std::function<void()>> m_callQueue;

  std::map<int, std::function<void()>> m_timerMap;
};

static QThreadStorage<AsyncQueue> threadLocalAsyncQueue;

template <class T>
struct AsyncRefPassHelper
{
  AsyncRefPassHelper(T&& v)
    : value(std::move(v))
  {}
  AsyncRefPassHelper(const AsyncRefPassHelper& o)
    : value(o.moveOut())
  {}
  AsyncRefPassHelper(AsyncRefPassHelper&& o)
    : value(o.moveOut())
  {}
  AsyncRefPassHelper& operator = (const AsyncRefPassHelper& o)
  {
    value = o.moveOut();
    return *this;
  }
  AsyncRefPassHelper& operator = (AsyncRefPassHelper&& o)
  {
    std::swap(value, o.value);
    return *this;
  }

  T&& moveOut() const
  {
    return std::move(value);
  }

  mutable T value;
};

struct QPromise::Pimpl : std::enable_shared_from_this<Pimpl>
{
  std::mutex m_mutex;
  Pimpl *m_upstream;
  std::vector<std::shared_ptr<Pimpl> > m_downstream;

  Value m_value;
  std::atomic<bool> m_closed;
  std::atomic<std::thread::id> m_resolved;
  std::atomic<bool> m_undeliveredException;

  std::unique_ptr<detail::CallbackWrapper> m_onFulfil;
  std::unique_ptr<detail::CallbackWrapper> m_onReject;

  int m_delayMs;
  bool m_asyncStarted;

  Pimpl()
    : m_upstream(nullptr)
    , m_value(Unset())
    , m_delayMs(-1)
    , m_asyncStarted(false)
  {
    m_closed.store(false, std::memory_order_relaxed);
    m_resolved.store(std::thread::id(), std::memory_order_relaxed);
    m_undeliveredException.store(false, std::memory_order_relaxed);
  }

  Pimpl(const Pimpl& other) = delete;

  ~Pimpl()
  {
    // Pass undelivered exceptions to the handler.
    if (m_undeliveredException.load(std::memory_order_relaxed) && undeliveredExceptionHandler)
    {
      std::atomic_thread_fence(std::memory_order_acquire);
      std::lock_guard<decltype(m_mutex)> lock(m_mutex);
      undeliveredExceptionHandler(m_value.cast<const std::exception_ptr&>());
    }
  }

  void close()
  {
    m_closed = true;
  }

  bool resolved() const
  {
    return m_resolved.load(std::memory_order_relaxed) != std::thread::id();
  }

  bool closed() const
  {
    return m_closed.load(std::memory_order_relaxed);
  }

  // Attach a downstream promise.
  void link(const std::shared_ptr<Pimpl>& next)
  {
    next->m_upstream = this;

    bool closed = false;
    std::thread::id resolved = m_resolved;
    if (resolved == std::thread::id()) {
      // The QPromise is probably not resolved (it could have resolved
      // immediately after the check). We need the lock to protect
      // against concurrent callers to this function and resolve().
      std::lock_guard<decltype(m_mutex)> lock(m_mutex);

      // Check type match between upstream callback result and
      // downstream callback argument. This check is inconclusive
      // if:
      //
      // - Callbacks are not present.
      // - Upstream result type is Any or QPromise.
      // - Downstream argument type is void.
      //
      // A mismatch would eventually be found in propagation but
      // it is much easier to debug when found during attachment.
      const std::type_info& oType =
        m_onFulfil ? m_onFulfil->resultType() :
        (m_onReject ? m_onReject->resultType() : typeid(detail::Any));
      const std::type_info& iType =
        next->m_onFulfil ? next->m_onFulfil->argumentType() : typeid(void);
      if (oType != iType &&
         oType != typeid(detail::Any) && oType != typeid(QPromise) &&
         iType != typeid(void))
      {
        throw std::logic_error(std::string("QPromise value type mismatch: ") + oType.name() + " -> " + iType.name());
      }

      m_downstream.push_back(next);
      if (m_value.type() != typeid(Unset))
      {
        // We can get here if the QPromise is resolved in between
        // testing m_resolved and obtaining the lock. If it did
        // happen, resolve() would have released the lock so
        // we can access m_value.
        resolved = m_resolved.load(std::memory_order_relaxed);
        if (m_value.type() == typeid(std::exception_ptr))
           m_undeliveredException.store(false, std::memory_order_relaxed);
      }

      // A QPromise is closed once an onFulfil callback with an rvalue
      // reference argument has been added because that callback can
      // steal (i.e. move) the value.
      //
      // Storing to the atomic m_closed has release semantics so
      // resolve() can access m_downstream without taking a lock as
      // long as it reads m_closed with acquire semantics.
      if (next->m_onFulfil && next->m_onFulfil->hasRvalueArgument())
      {
        m_closed.store(true, std::memory_order_release);
        closed = true;
      }
    }
    else
    {
      // This QPromise already has a value so next can immediately
      // be resolved with it. A lock is not neccessary as downstream_
      // is not used and the previous access of m_resolved acquires
      // value_.
      if (m_value.type() == typeid(std::exception_ptr))
        m_undeliveredException.store(false, std::memory_order_relaxed);

      // A QPromise is closed once an onFulfil callback with an rvalue
      // reference argument has been added because that callback can
      // steal (i.e. move) the value.
      //
      // Release semantics for m_closed are unnecessary because
      // resolve() has already been called and m_downstream is not
      // modified.
      if (next->m_onFulfil && next->m_onFulfil->hasRvalueArgument())
      {
        m_closed.store(true, std::memory_order_relaxed);
        closed = true;
      }
    }

    if (resolved != std::thread::id())
    {
      std::unique_lock<decltype(m_mutex)> lock(m_mutex, std::defer_lock);
      if (closed && resolved != std::this_thread::get_id())
      {
        // This is the problem case where this call has added an
        // onResolve with an rvalue reference argument to a
        // resolved QPromise. We have to ensure that we wait until
        // any in-progress resolvement completes, or else the value
        // could be stolen from it.
        lock.lock();
      }

      // It is possible for two threads to execute this statement
      // concurrently, which can cause a problem if (1) one of the
      // calls closed the QPromise, and (2) the same call steals the
      // value before the other uses it. This would be a race bug
      // in user code even with synchronization, however, as it
      // would be arbitrary whether the closing call came first
      // (making the other call invalid), or second (making the
      // other call valid).
      next->resolve(std::move(m_value), false);
    }
  }

  // Set promise value.
  void resolve(Value&& value, bool direct)
  {
    if (direct && (m_value.type() != typeid(Unset) || m_asyncStarted))
      throw std::logic_error("QPromise already resolved");


    if (m_delayMs >= 0 && !m_asyncStarted)
    {
      m_asyncStarted = true;

      std::shared_ptr<Pimpl> self(this->shared_from_this());
      AsyncRefPassHelper<detail::Any> valueMoveHelper(std::move(value));

      threadLocalAsyncQueue.localData().callLater(m_delayMs,
          [self, valueMoveHelper]()
          {
            self->resolve(valueMoveHelper.moveOut(), false);
          });

      return;
    }

    m_asyncStarted = false;

    // Pass value through appropriate callback if present.
    Value cbValue{Unset()};
    if (m_onFulfil && value.type() != typeid(std::exception_ptr))
    {
      try
      {
        cbValue = (*m_onFulfil)(std::move(value));
      }
      catch (const bad_cast& e)
      {
        // The type contained in the Value does not match the
        // type the onFulfil callback accepts, so this is a
        // user code error.
        if (badCastHandler)
           badCastHandler(e);

        cbValue = std::current_exception();
      }
      catch (...)
      {
        // All other exceptions are propagated downstream.
        cbValue = std::current_exception();
      }
    }
    else if (m_onReject && value.type() == typeid(std::exception_ptr))
    {
      try
      {
        cbValue = (*m_onReject)(std::move(value));
      }
      catch (...)
      {
        cbValue = std::current_exception();
      }
    }

    if (cbValue.type() != typeid(QPromise))
    {
      // Access to m_downstream is exclusive when closed so the
      // lock can be avoided in that state (testing m_closed has
      // acquire semantics so m_downstream is valid). When not
      // closed there is a possible race with link() so the lock
      // is required.
      const bool closed = m_closed;
      std::unique_lock<decltype(m_mutex)> lock(m_mutex, std::defer_lock);
      if (!closed)
        lock.lock();

      if (direct && m_upstream)
        throw std::logic_error("invalid operation on dependent QPromise");

      m_upstream = nullptr;

      // If a callback transformed the value, move it.
      // If the value came from the user, move it.
      // If the value came from upstream, copy it.
      if (cbValue.type() != typeid(Unset))
        m_value.swap(cbValue);
      else if (direct)
        m_value.swap(value);
      else
        m_value = value;

      // Local update is complete. This store has release semantics
      // so threads that acquire m_resolved can access updates to m_value.
      m_resolved = std::this_thread::get_id();

      if (!m_downstream.empty())
      {
        // Propagate resolvement to dependent Promises.
        for (const auto& child : m_downstream)
          child->resolve(std::move(m_value), false);
      }
      else if (m_value.type() == typeid(std::exception_ptr))
      {
        // The value contains an undelivered exception. If it
        // remains undelivered at destruction then the handler
        // will be called, potentially in a different thread. We
        // ensure that the value will be valid there by specifying
        // release semantics if the lock was not used.
        m_undeliveredException.store(
           true,
           closed ? std::memory_order_release : std::memory_order_relaxed);
      }
    }
    else
    {
      // Discard callbacks so they cannot be used again.
      m_onFulfil.reset();
      m_onReject.reset();

      // Make a returned QPromise the new upstream.
      auto& p = cbValue.cast<QPromise&>();
      p.pimpl->link(shared_from_this());
    }
  }
};

QPromise::QPromise()
   : pimpl(std::make_shared<Pimpl>())
{
  // STL containers will copy instead of move if they can't guarantee
  // strong exception safety. These checks are sufficient for that.
  static_assert(std::is_nothrow_move_constructible<QPromise>::value, "noexcept move");
  static_assert(std::is_nothrow_move_assignable<QPromise>::value, "noexcept assign");
}

QPromise::QPromise(QPromise&& other) Q_DECL_NOTHROW
   : pimpl(std::make_shared<Pimpl>())
{
  pimpl.swap(other.pimpl);
}

QPromise::QPromise(detail::CallbackWrapper *onFulfil, detail::CallbackWrapper *onReject, int delayMs)
   : pimpl(std::make_shared<Pimpl>())
{
  pimpl->m_onFulfil.reset(onFulfil);
  pimpl->m_onReject.reset(onReject);
  if (delayMs >= 0)
    pimpl->m_delayMs = delayMs;
}

QPromise::~QPromise() Q_DECL_NOTHROW
{
}

QPromise& QPromise::close()
{
  pimpl->close();
  return *this;
}

const QPromise& QPromise::close() const
{
  pimpl->close();
  return *this;
}

bool QPromise::resolved() const
{
  return pimpl->resolved();
}

bool QPromise::closed() const
{
  return pimpl->closed();
}

QPromise::ExceptionHandler QPromise::setUndeliveredExceptionHandler(const ExceptionHandler& handler)
{
  ExceptionHandler previous = std::move(undeliveredExceptionHandler);
  undeliveredExceptionHandler = handler;
  return previous;
}

QPromise::BadCastHandler QPromise::setBadCastExceptionHandler(const BadCastHandler& handler)
{
  BadCastHandler previous = std::move(badCastHandler);
  badCastHandler = handler;
  return previous;
}

void QPromise::resolve(Value&& value) const
{
  pimpl->resolve(std::move(value), true);
}

void QPromise::attach(const QPromise& next) const
{
  pimpl->link(next.pimpl);
}

QPromise QPromise::deferToThreadPool(detail::CallbackWrapper* callable, QThreadPool* pool)
{
  std::unique_ptr<detail::CallbackWrapper> callback(callable);

  QPromise rv;
  if (!pool)
    pool = QThreadPool::globalInstance();

  class Runnable: public QRunnable
  {
  public:
    Runnable(AsyncQueue* queue, std::unique_ptr<detail::CallbackWrapper>&& callable, const QPromise& promise)
      : m_callable(std::move(callable))
      , m_promise(promise)
      , m_reported(false)
      , m_queue(queue)
    {
      setAutoDelete(true);
    }
    virtual ~Runnable()
    {
      if (!m_reported)
        m_promise.reject(std::runtime_error("QPromise thread execution cancelled"));
    }

    virtual void run() override
    {
      Value cbValue{Unset()};
      try
      {
        cbValue = (*m_callable)(detail::Any());
      }
      catch (const bad_cast& e)
      {
        // The type contained in the Value does not match the
        // type the onFulfil callback accepts, so this is a
        // user code error.
        if (badCastHandler)
           badCastHandler(e);

        cbValue = std::current_exception();
      }
      catch (...)
      {
        // All other exceptions are propagated downstream.
        cbValue = std::current_exception();
      }
      m_reported = true;

      QPromise promise = m_promise;
      AsyncRefPassHelper<detail::Any> valueMoveHelper(std::move(cbValue));
      m_queue->callLater(
          [promise, valueMoveHelper]()
          {
            promise.resolve(valueMoveHelper.moveOut());
          });
    }

    std::unique_ptr<detail::CallbackWrapper> m_callable;
    QPromise m_promise;
    bool m_reported;
    AsyncQueue* m_queue;
  };

  pool->start(new Runnable(&threadLocalAsyncQueue.localData(), std::move(callback), rv));
  return std::move(rv);
}

QPromise QPromise::deferToThread(detail::CallbackWrapper* callable)
{
  QPromise promise;

  AsyncQueue* currentQueue = &threadLocalAsyncQueue.localData();
  AsyncRefPassHelper<std::unique_ptr<detail::CallbackWrapper>> callbackRef{std::unique_ptr<detail::CallbackWrapper>(callable)};

  auto thread = std::thread(
        [currentQueue, promise, callbackRef]()
        {
          Value cbValue{Unset()};
          try
          {
            cbValue = (*callbackRef.value.get())(detail::Any());
          }
          catch (const bad_cast& e)
          {
            // The type contained in the Value does not match the
            // type the onFulfil callback accepts, so this is a
            // user code error.
            if (badCastHandler)
               badCastHandler(e);

            cbValue = std::current_exception();
          }
          catch (...)
          {
            // All other exceptions are propagated downstream.
            cbValue = std::current_exception();
          }

          AsyncRefPassHelper<detail::Any> valueMoveHelper(std::move(cbValue));
          currentQueue->callLater(
              [promise, valueMoveHelper]()
              {
                promise.resolve(valueMoveHelper.moveOut());
              });
        });
  thread.detach();

  return promise;
}


#include "QPromise.moc"
