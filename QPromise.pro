TEMPLATE = lib
QT = core

# Build as static library
CONFIG += staticlib

# Build as dynamic library
#DEFINES += QPROMISE_DLL_BUILD

# When you are looking for a library copmile location :)
#DESTDIR = $$PWD/output

INCLUDEPATH += include

SOURCES += \
	QPromise.cpp
	
HEADERS += \
	include/QPromise.h \
	include/QPromise_impl.h
