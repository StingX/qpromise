/** \file QPromise_impl.h
  * \brief Implementation details of the Promises/A+ - like QPromise class
  * \author Devyatnikov A.
  * \date 21.03.2016
  * Project: QPromise
  *
  * This is an adaptation from https://github.com/rhashimoto/poolqueue
  */

#ifndef QPROMISE_IMPL_H
#define QPROMISE_IMPL_H

#include <QObject>

#include <exception>
#include <stdexcept>
#include <string>
#include <typeinfo>
#include <utility>
#include <vector>

namespace detail {

class bad_cast: public std::bad_cast
{
  const std::type_info& m_from;
  const std::type_info& m_to;
  std::string m_message;
public:
  bad_cast(const std::type_info& from, const std::type_info& to)
    : m_from(from)
    , m_to(to)
    , m_message(std::string("failed cast from ") + from.name() + " to " + to.name())
  {
  }

  const std::type_info& from() const
  {
    return m_from;
  }

  const std::type_info& to() const
  {
    return m_to;
  }

  virtual const char *what() const Q_DECL_NOTHROW override
  {
    return m_message.c_str();
  }
};

class Any {

  class Holder
  {
  public:
    virtual ~Holder() {}
    virtual const std::type_info& type() const = 0;
    virtual Holder *copy() const = 0;
  };

  template<typename T, bool CopyConstructible>
  class HolderT : public Holder
  {
    T m_value;

  public:
    HolderT(const T& value)
      : m_value(value)
    {}

    HolderT(T&& value)
      : m_value(std::forward<T>(value))
    {}

    const std::type_info& type() const
    {
       return typeid(T);
    }

    Holder *copy() const
    {
       return new HolderT(m_value);
    }

    T& get()
    {
       return m_value;
    }

    const T& get() const
    {
       return m_value;
    }
  };

  // Specialization for non-copy-constructible values.
  template<typename T>
  class HolderT<T, false>: public Holder
  {
    T m_value;

  public:
    HolderT(T&& value)
      : m_value(std::forward<T>(value))
    {}

    const std::type_info& type() const
    {
       return typeid(T);
    }

    Holder *copy() const
    {
       // Copying a QPromise::Value is necessary if a then()
       // or except() callback returns a QPromise. When the
       // returned QPromise is fulfilled, the value set must
       // be copy-constructible. Noncopyable values can be
       // used in all other known internal cases.
      throw std::runtime_error("QPromise contains noncopyable value");
    }

    T& get()
    {
       return m_value;
    }

    const T& get() const
    {
       return m_value;
    }
  };

  Holder *m_holder;
public:
  Any()
    : m_holder(nullptr)
  {
  }

  ~Any()
  {
    delete m_holder;
  }

  // Copy constructor and assignment.
  Any(const Any& other)
   : m_holder(other.m_holder ? other.m_holder->copy() : other.m_holder)
  {
  }

  Any& operator= (const Any& other)
  {
    return swap(Any(other));
  }

  // Move construction and assignment.
  Any(Any&& other)
    : m_holder(nullptr)
  {
    swap(other);
  }

  Any& operator= (Any&& other)
  {
    return swap(other);
  }

  // Value construction and assignment. Use SFINAE to
  // exclude matching an Any argument.
  template<typename T,
          typename = typename std::enable_if<!std::is_same<typename std::decay<T>::type, Any>::value, T>::type>
  Any(T&& value)
   : m_holder(new HolderT<typename std::decay<T>::type, std::is_copy_constructible<typename std::decay<T>::type>::value>(std::forward<T>(value)))
  {}

  template<typename T,
          typename = typename std::enable_if<!std::is_same<typename std::decay<T>::type, Any>::value, T>::type>
  Any& operator= (T&& value)
  {
    return swap(Any(std::forward<T>(value)));
  }

  // Swap (both lvalue and rvalue).
  Any& swap(Any& other)
  {
    std::swap(m_holder, other.m_holder);
    return *this;
  }

  Any& swap(Any&& other)
  {
    std::swap(m_holder, other.m_holder);
    return *this;
  }

  // Query wrapped type.
  const std::type_info& type() const
  {
    return m_holder ? m_holder->type() : typeid(void);
  }

  bool empty() const
  {
    return !m_holder;
  }

  // Value cast for non-const instance.
  template<typename T>
  T cast()
  {
    typedef typename std::decay<T>::type DecayType;
    Q_CONSTEXPR bool isCopyable = std::is_copy_constructible<DecayType>::value;
    auto *holder = dynamic_cast<HolderT<DecayType, isCopyable> *>(m_holder);
    if (!holder)
      throw bad_cast(type(), typeid(DecayType));
    return static_cast<T>(holder->get());
  }

  // Const reference cast for const instance.
  template<typename T>
  const T& cast() const
  {
    typedef typename std::decay<T>::type DecayType;
    constexpr bool isCopyable = std::is_copy_constructible<DecayType>::value;
    auto *holder = dynamic_cast<const HolderT<DecayType, isCopyable> *>(m_holder);
    if (!holder)
      throw bad_cast(type(), typeid(DecayType));
    return static_cast<const T&>(holder->get());
  }
};

// Enable idiomatic swap.
inline void swap(Any& a, Any& b)
{
  a.swap(b);
}

// Define trait templates for functions and member functions.
// See:
//  http://functionalcpp.wordpress.com/2013/08/05/function-traits/
//  https://smellegantcode.wordpress.com/tag/c11/
template<typename F> struct FunctionTraits;
template<typename T> struct MemberFunctionTraits;

// Specializations for 0 and 1 argument functions.
template<typename R>
struct FunctionTraits<R()>
{
  typedef R ResultType;
  typedef void ArgumentType;
};

template<typename R, typename A>
struct FunctionTraits<R(A)>
{
  typedef R ResultType;
  typedef A ArgumentType;
};

// Adapt function pointers.
template<typename R>
struct FunctionTraits<R(*)()> : public FunctionTraits<R()>
{
};

template<typename R, typename A>
struct FunctionTraits<R(*)(A)> : public FunctionTraits<R(A)>
{
};

// Specializations for 0 and 1 argument member functions.
template<typename R, typename C>
struct MemberFunctionTraits<R (C::*)() const>
{
  typedef R ResultType;
  typedef void ArgumentType;
};

template<typename R, typename C, typename A>
struct MemberFunctionTraits<R (C::*)(A) const>
{
  typedef R ResultType;
  typedef A ArgumentType;
};

// Functor traits.
template<typename F>
class FunctorTraits
{
  typedef typename std::decay<F>::type PlainF;
  typedef MemberFunctionTraits<decltype(&PlainF::operator())> traits;
public:
  typedef typename traits::ResultType ResultType;
  typedef typename traits::ArgumentType ArgumentType;
};

// Combine function and functor traits.
template<typename F>
struct CallableTraits: public
    std::conditional<
      std::is_function<typename std::remove_pointer<F>::type>::value,
      FunctionTraits<F>,
      FunctorTraits<F>
    >::type
{
};

class CallbackWrapper
{
public:
  virtual ~CallbackWrapper() {}

  virtual const std::type_info& argumentType() const = 0;
  virtual const std::type_info& resultType() const = 0;
  virtual bool hasRvalueArgument() const = 0;
  virtual bool hasExceptionPtrArgument() const = 0;
  virtual Any operator()(Any&&) const = 0;
};

// Template subclass wrapper for R f(A).
template<typename F, typename R, typename A, bool IsValue, bool IsVector, bool IsTuple>
class CallbackWrapperT : public CallbackWrapper
{
  typename std::remove_reference<F>::type f_;
public:
  CallbackWrapperT(F&& f)
    : f_(std::forward<F>(f))
  {}

  const std::type_info& argumentType() const
  {
    return typeid(typename std::decay<A>::type);
  }

  const std::type_info& resultType() const
  {
    return typeid(typename std::decay<R>::type);
  }

  bool hasRvalueArgument() const
  {
    return std::is_rvalue_reference<A>::value;
  }

  bool hasExceptionPtrArgument() const
  {
    return std::is_convertible<A, const std::exception_ptr&>::value;
  }

  Any operator()(Any&& a) const
  {
    typedef typename std::conditional<std::is_rvalue_reference<A>::value, A&&, const A&>::type type;
    return f_(a.cast<type>());
  }
};

// Specialize for void f(A).
template<typename F, typename A>
class CallbackWrapperT<F, void, A, false, false, false> : public CallbackWrapper
{
   typename std::remove_reference<F>::type f_;
public:
  CallbackWrapperT(F&& f)
   : f_(std::forward<F>(f))
  {}

  const std::type_info& argumentType() const
  {
    return typeid(typename std::decay<A>::type);
  }

  const std::type_info& resultType() const
  {
    return typeid(void);
  }

  bool hasRvalueArgument() const
  {
    return std::is_rvalue_reference<A>::value;
  }

  bool hasExceptionPtrArgument() const
  {
    return std::is_convertible<A, const std::exception_ptr&>::value;
  }

  Any operator()(Any&& a) const
  {
    typedef typename std::conditional<std::is_rvalue_reference<A>::value, A&&, const A&>::type type;
    f_(a.cast<type>());
    return Any();
  }
};

// Specialize for R f().
template<typename F, typename R>
class CallbackWrapperT<F, R, void, false, false, false> : public CallbackWrapper
{
  typename std::remove_reference<F>::type f_;
public:
  CallbackWrapperT(F&& f)
    : f_(std::forward<F>(f))
  {}

  const std::type_info& argumentType() const
  {
    return typeid(void);
  }

  const std::type_info& resultType() const
  {
    return typeid(typename std::decay<R>::type);
  }

  bool hasRvalueArgument() const
  {
    return false;
  }

  bool hasExceptionPtrArgument() const
  {
    return false;
  }

  Any operator()(Any&&) const
  {
    return f_();
  }
};

#if 0
// Specialize for void f().
template<typename F>
class CallbackWrapperT<F, void, void, false, false, false> : public CallbackWrapper
{
  typename std::remove_reference<F>::type f_;
public:
  CallbackWrapperT(F&& f)
   : f_(std::forward<F>(f))
  {}

  const std::type_info& argumentType() const
  {
    return typeid(void);
  }

  const std::type_info& resultType() const
  {
    return typeid(void);
  }

  bool hasRvalueArgument() const
  {
    return false;
  }

  bool hasExceptionPtrArgument() const
  {
    return false;
  }

  Any operator()(Any&&) const
  {
    f_();
    return Any();
  }
};
#endif

// Specialize for R f(const Any&) or R f(Any&&).
template<typename F, typename R, typename A>
class CallbackWrapperT<F, R, A, true, false, false> : public CallbackWrapper {
  typename std::remove_reference<F>::type f_;
public:
  CallbackWrapperT(F&& f)
   : f_(std::forward<F>(f))
  {}

  const std::type_info& argumentType() const
  {
    return typeid(Any);
  }

  const std::type_info& resultType() const
  {
    return typeid(typename std::decay<R>::type);
  }

  bool hasRvalueArgument() const
  {
    return std::is_rvalue_reference<A>::value;
  }

  bool hasExceptionPtrArgument() const
  {
    return false;
  }

  Any operator()(Any&& a) const
  {
    return f_(static_cast<A>(a));
  }
};

// Specialize for void f(const Any&) or void f(Any&&).
template<typename F, typename A>
class CallbackWrapperT<F, void, A, true, false, false> : public CallbackWrapper
{
  typename std::remove_reference<F>::type f_;
public:
  CallbackWrapperT(F&& f)
   : f_(std::forward<F>(f))
  {}

  const std::type_info& argumentType() const
  {
    return typeid(Any);
  }

  const std::type_info& resultType() const
  {
    return typeid(void);
  }

  bool hasRvalueArgument() const
  {
    return std::is_rvalue_reference<A>::value;
  }

  bool hasExceptionPtrArgument() const
  {
    return false;
  }

  Any operator()(Any&& a) const
  {
    f_(static_cast<A>(a));
    return Any();
  }
};

// Specialize for R f(const std::vector<T>&) or R f(std::vector<T>&&).
template<typename F, typename R, typename A>
class CallbackWrapperT<F, R, A, false, true, false> : public CallbackWrapper
{
  typename std::remove_reference<F>::type f_;
public:
  CallbackWrapperT(F&& f)
    : f_(std::forward<F>(f))
  {}

  const std::type_info& argumentType() const
  {
    return typeid(typename std::decay<A>::type);
  }

  const std::type_info& resultType() const
  {
    return typeid(typename std::decay<R>::type);
  }

  bool hasRvalueArgument() const
  {
    return std::is_rvalue_reference<A>::value;
  }

  bool hasExceptionPtrArgument() const
  {
    return false;
  }

  Any operator()(Any&& a) const
  {
    if (a.type() != typeid(std::vector<Any>))
    {
      typedef typename std::conditional<std::is_rvalue_reference<A>::value, A&&, const A&>::type type;
      return f_(a.cast<type>());
    }
    else
    {
      typedef typename std::decay<A>::type ABase;
      typedef typename ABase::value_type ElementType;
      std::vector<Any>& av = a.cast<std::vector<Any>&>();
      const auto n = av.size();
      ABase v(n);

      if (hasRvalueArgument())
      {
        for (size_t i = 0; i < n; ++i)
          v[i] = std::move(av[i].cast<ElementType&>());
      }
      else
      {
        for (size_t i = 0; i < n; ++i)
          v[i] = av[i].cast<const ElementType&>();
      }
      return f_(std::move(v));
    }
  }
};

// Specialize for void f(const std::vector<T>&) or void f(std::vector<T>&&).
template<typename F, typename A>
class CallbackWrapperT<F, void, A, false, true, false> : public CallbackWrapper
{
  typename std::remove_reference<F>::type f_;
public:
  CallbackWrapperT(F&& f)
   : f_(std::forward<F>(f))
  {}

  const std::type_info& argumentType() const
  {
    return typeid(typename std::decay<A>::type);
  }

  const std::type_info& resultType() const
  {
    return typeid(void);
  }

  bool hasRvalueArgument() const
  {
    return std::is_rvalue_reference<A>::value;
  }

  bool hasExceptionPtrArgument() const
  {
    return false;
  }

  Any operator()(Any&& a) const
  {
    if (a.type() != typeid(std::vector<Any>))
    {
      typedef typename std::conditional<std::is_rvalue_reference<A>::value, A&&, const A&>::type type;
      f_(a.cast<type>());
    }
    else
    {
      typedef typename std::decay<A>::type ABase;
      typedef typename ABase::value_type ElementType;
      std::vector<Any>& av = a.cast<std::vector<Any>&>();
      const auto n = av.size();
      ABase v(n);

      if (hasRvalueArgument())
      {
        for (size_t i = 0; i < n; ++i)
          v[i] = std::move(av[i].cast<ElementType&>());
      }
      else
      {
        for (size_t i = 0; i < n; ++i)
          v[i] = av[i].cast<const ElementType&>();
      }
      f_(std::move(v));
    }

    return Any();
  }
};

template<std::size_t I = 0, typename... Tp>
inline typename std::enable_if<I == sizeof...(Tp), void>::type
copy_tuple(std::tuple<Tp...>&, std::vector<Any>::const_iterator)
{
}

template<std::size_t I = 0, typename... Tp>
inline typename std::enable_if<I < sizeof...(Tp), void>::type
copy_tuple(std::tuple<Tp...>& t, std::vector<Any>::const_iterator i)
{
   typedef typename std::tuple_element<I, std::tuple<Tp...> >::type ElementType;
   std::get<I>(t) = i->cast<const ElementType&>();
   copy_tuple<I + 1, Tp...>(t, ++i);
}

template<std::size_t I = 0, typename... Tp>
inline typename std::enable_if<I == sizeof...(Tp), void>::type
move_tuple(std::tuple<Tp...>&, std::vector<Any>::iterator)
{
}

template<std::size_t I = 0, typename... Tp>
inline typename std::enable_if<I < sizeof...(Tp), void>::type
move_tuple(std::tuple<Tp...>& t, std::vector<Any>::iterator i)
{
  typedef typename std::tuple_element<I, std::tuple<Tp...> >::type ElementType;
  std::get<I>(t) = std::move(i->cast<ElementType&>());
  move_tuple<I + 1, Tp...>(t, ++i);
}

// Specialize for R f(const std::tuple<T0, T1...>&) or R f(std::tuple<T0, T1...>&&).
template<typename F, typename R, typename A>
class CallbackWrapperT<F, R, A, false, false, true> : public CallbackWrapper {
  typename std::remove_reference<F>::type f_;
public:
  CallbackWrapperT(F&& f)
    : f_(std::forward<F>(f))
  {}

  const std::type_info& argumentType() const
  {
    return typeid(typename std::decay<A>::type);
  }

  const std::type_info& resultType() const
  {
    return typeid(typename std::decay<R>::type);
  }

  bool hasRvalueArgument() const
  {
    return std::is_rvalue_reference<A>::value;
  }

  bool hasExceptionPtrArgument() const
  {
    return false;
  }

  Any operator()(Any&& a) const
  {
    if (a.type() != typeid(std::vector<Any>))
    {
      typedef typename std::conditional<std::is_rvalue_reference<A>::value, A&&, const A&>::type type;
      return f_(a.cast<type>());
    }
    else
    {
      typedef typename std::decay<A>::type ABase;
      std::vector<Any>& at = a.cast<std::vector<Any>&>();
      ABase t;

      if (hasRvalueArgument())
        move_tuple(t, at.begin());
      else
        copy_tuple(t, at.begin());

      return f_(std::move(t));
    }
  }
};

// Specialize for void f(const std::tuple<T0, T1...>&) or void f(std::tuple<T0, T1...>&&).
template<typename F, typename A>
class CallbackWrapperT<F, void, A, false, false, true> : public CallbackWrapper {
  typename std::remove_reference<F>::type f_;
public:
  CallbackWrapperT(F&& f)
    : f_(std::forward<F>(f))
  {}

  const std::type_info& argumentType() const
  {
    return typeid(typename std::decay<A>::type);
  }

  const std::type_info& resultType() const
  {
    return typeid(void);
  }

  bool hasRvalueArgument() const
  {
    return std::is_rvalue_reference<A>::value;
  }

  bool hasExceptionPtrArgument() const
  {
    return false;
  }

  Any operator()(Any&& a) const
  {
    if (a.type() != typeid(std::vector<Any>))
    {
       typedef typename std::conditional<std::is_rvalue_reference<A>::value, A&&, const A&>::type type;
       f_(a.cast<type>());
    }
    else
    {
       typedef typename std::decay<A>::type ABase;
       std::vector<Any>& at = a.cast<std::vector<Any>&>();
       ABase t;

       if (hasRvalueArgument())
          move_tuple(t, at.begin());
       else
          copy_tuple(t, at.begin());

       f_(std::move(t));
    }

    return Any();
  }
};

template<template<typename...> class TT, typename T>
struct is_instantiation_of : std::false_type {
};

template<template<typename...> class TT, typename... Ts>
struct is_instantiation_of<TT, TT<Ts...> > : std::true_type {
};

template<typename F>
CallbackWrapper *makeCallbackWrapper(F&& f) {
  typedef typename CallableTraits<F>::ResultType R;
  typedef typename CallableTraits<F>::ArgumentType A;

  Q_CONSTEXPR bool isConst = std::is_const<typename std::remove_reference<A>::type>::value;
  Q_CONSTEXPR bool isLvalue = std::is_lvalue_reference<A>::value;
  static_assert(isConst || !isLvalue,
               "QPromise callbacks cannot take non-const lvalue reference argument.");

  Q_CONSTEXPR bool isValue = std::is_same<typename std::decay<A>::type, Any>::value;
  Q_CONSTEXPR bool isValueConstRef = std::is_same<A, const Any&>::value;
  Q_CONSTEXPR bool isValueRvalueRef = std::is_rvalue_reference<A>::value;
  static_assert(!isValue || isValueConstRef || isValueRvalueRef,
               "QPromise callback should take const Value& or Value&&.");

  Q_CONSTEXPR bool isVector = is_instantiation_of<std::vector, typename std::decay<A>::type>::value;
  Q_CONSTEXPR bool isTuple = is_instantiation_of<std::tuple, typename std::decay<A>::type>::value;

  Q_CONSTEXPR bool isException = std::is_same<typename std::decay<A>::type, std::exception_ptr>::value;
  Q_CONSTEXPR bool isExceptionConstRef = std::is_same<A, const std::exception_ptr&>::value;
  static_assert(!isException || isExceptionConstRef,
               "QPromise callback should take const std::exception_ptr&.");

  return new CallbackWrapperT<F, typename std::decay<R>::type, A, isValue, isVector, isTuple>(std::forward<F>(f));
}

struct Null {
};

struct NullFulfil {
  Null operator()() const {
    return Null();
  }
};

struct NullReject {
  Null operator()(const std::exception_ptr&) const {
    return Null();
  }
};

} // end namespace detail

#endif // QPROMISE_IMPL_H

