INCLUDEPATH += $$PWD/include
SOURCES += $$PWD/QPromise.cpp

# If you are bulding your dynamic library, define this in your PRO file
# and all the PRO files, that uses your library. Assuming MY_LIB_EXPORT
# expands to Q_DECL_EXPORT when you build your library and to Q_DECL_IMPORT when
# you build other modules, that links to your library.
#DEFINES += QPROMISE_EXPORT=MY_LIB_EXPORT
