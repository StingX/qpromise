# QPromise

QPromise is a C++ library for asynchronous operations. Its primary
feature is asynchronous promises, inspired by the [Javascript
Promises/A+ specification](https://promisesaplus.com/). The library
uses Qt 5.x toolkit QEventLoop and QThreadPool to provide true asynchronious
operation. The library implementation makes use of C++11 features and depends
only on QtCore (and QtTest for tests).

The key distinction between Promises/A+ and `std::promise` in C++11 is
that Promises/A+ provides *non-blocking* synchronization (via chaining
function objects) and `std::promise` provides *blocking*
synchronization (or polling). Both have their uses and one is not a
direct replacement for the other.

`boost::future` provides a [`then()` method](http://www.boost.org/doc/libs/1_58_0/doc/html/thread/synchronization.html#thread.synchronization.futures.then)
which is non-blocking and is closer in spirit to QPromise, but there
are a number of key differences. The `boost::future` implementation of
`then()` spawns a thread to block on the future, while QPromise
simply chains a callback. Returning a Promise from a callback is an
important Promises/A+ feature, but is not allowed with `boost::future`.
The `then()` method can be invoked at most once on a `boost::future`
instance, while Promises/A+ does not have this restriction.

The core of the QPromise implementation is borrowed from [poolqueue](https://github.com/rhashimoto/poolqueue). The code is available
under the [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

The library is checked to compile with:

 - Microsoft Visual Studio 2013
 - GCC 4.9.2
 - Qt 5.6.0

## Promise overview
The [Javascript
Promises/A+ specification](https://promisesaplus.com/) describes a promise:

> A promise represents the eventual result of an asynchronous
> operation. The primary way of interacting with a promise is through
> its then method, which registers callbacks to receive either a
> promise's eventual value or the reason why the promise cannot be
> fulfilled.

A `QPromise` can be in one of three states: *pending* (aka
not resolved), *fulfilled*, or *rejected*. Pending means that the
`QPromise` has not yet been fulfilled or rejected. Fulfilled means that
the `QPromise` has a value (possibly `void`). Rejected means that the
`QPromise` has an exception.

A `QPromise` can have function callbacks to invoke when it is resolved
(either fulfilled or rejected). Whichever callback is invoked, the
`QPromise` will be fulfilled if the callback returns or rejected if the
callback throws an exception. The exception to this is if the callback
returns a `QPromise`. In this case the original `QPromise` will resolve
with the result of the returned `QPromise` (callbacks will not be
invoked again).

A `QPromise` can have dependent `QPromise`s created and attached using
the `then()`, `thenSync()`, `except()` or `exceptSync()` methods. 
When a `QPromise` is resolved, it
invokes the appropriate callback if present, and then recursively
resolves dependent `QPromise`s with the result (value or exception). A
dependent `QPromise` newly attached to an already resolved `QPromise`
will be resolved immediately. The callbacks attached with `then()` and `except()`
are always called asynchroniously using the Qt event loop in the thread,
that performed this attachment. The callbacks attached with `thenSync()`
and `exceptSync()` are always called synchroniously as soon, as the
`QPromise` get resolved in the context of the resolving thread.

Example:

    #include "QPromise.h"
    #include <QDebug>
    ...
    QPromise p;
    p.then(
      [](const QString& s) 
      {
        qDebug() << "fulfilled with" << s;
        return nullptr;
      },
      [](const std::exception_ptr& e) 
      {
        try { std::rethrow_exception(e); }
        catch (const std::exception& e)
        {
          qDebug() << "rejected with" << QString::fromLocal8Bit(e.what());
        }
        return nullptr;
      });
    ...
    // possibly in another thread
    p.resolve(QStringLiteral("how now brown cow"));

The lambdas attached with `then()` are deferred until the `QPromise` is
resolved.

## Promise details
A `QPromise` holds a shared pointer to its state. Copying a
`QPromise` produces another reference to the same state, not a brand
new `QPromise`. This allows lambdas to capture `QPromise`s by value.

Note that unlike Javascript, a `QPromise` callback must
return a value. This requirement helps to avoid a common programming
mistake that is hard to debug. If a callback does not compute a
meaningful result then a dummy value, e.g. `nullptr`, can be returned.

A `QPromise` can be closed to `then()` and `except()` methods, either
explicitly using `close()` or implicitly by passing an `onFulfil`
callback to `then()` that takes an rvalue reference argument. Closed
`QPromises`s may resolve slightly faster (due to an internal
optimization), plus moving a value from an rvalue reference avoids a
copy.

The static method `QPromise::all()` can be used to create a new
`QPromise` dependent on an input set of `QPromise`s. The new `QPromise`
fulfils (with an empty value) when all the input `QPromise`s fulfil, or
rejects when any of the input `QPromise`s reject (with the exception
from the first to reject).

The static method `QPromise::any()` also creates a new `QPromise`
dependent on an input set of `QPromise`s. The new `QPromise` fulfils
when any of the input `QPromise`s fulfil (with the value from the
first to fulfil), or rejects when all of the input `QPromise`s reject.

A rejected Promise that never delivers its exception to an `onReject`
callback will invoke an undelivered exception handler in its
destructor. The default handler calls `std::unexpected()`, which helps
to catch errors that are mistakenly being ignored. The default handler
can be replaced with `QPromise::setUndeliveredExceptionHandler()`.

The library attempts to report type mismatches within calls to
`then()` or `except()` by throwing `std::logic_error` but this is not
always possible (e.g. when a callback returns a `QPromise`). When a
type mismatch occurs when trying invoke a callback, a bad cast handler
is invoked. The default handler throws `QPromise::bad_cast`. The
default handler can be replaced with
`QPromise::setBadCastExceptionHandler()`. If the replacement handler
does not throw then the exception will be captured just like any other
callback exception.

## Delay
The benefits of `QPromise`s don't become apparent until you have
asynchronous services that return them. `QPromise::delay` is a simple but
useful service to create a `QPromise` that calls user function when a timer
expires:

    #include "QPromise.h"
    #include <QDebug>
    ...
    QPromise::delay(5000)
      .thenSync(
        []() 
        {
          qDebug() << "I waited.";
          return nullptr;
        });

## ThreadPool
QPromise also may utilize Qt QThreadPool.  Passing a
callable object to `QPromise::deferToThreadPool` adds it to a queue and returns
a `QPromise` that is resolved with the result of the callable object
when it is executed on one of the threads in the pool:

    #include "QPromise.h"
    #include <QDebug>
    ...
    QPromise p = QPromise::deferToThreadPool(
      []() 
      {
        qDebug() << "I'm running in the pool.";
        return QString("my data");
      });
      
    p.then(
      [](QString&& s) 
      {
        qDebug() << "Worker result is" << s;
        return nullptr;
      },
      [](const std::exception& e) 
      {
        qDebug() << "Worker threw an exception";
        return nullptr;
      });


## Floating thread
QPromise may execute a callable in the newly created thread. Passing a
callable object to `QPromise::deferToThread` starts a new thread and returns
a `QPromise` that is resolved with the result of the callable object
when it is executed in that thread:

    #include "QPromise.h"
    #include <QDebug>
    ...
    QPromise p = QPromise::deferToThread(
      []() 
      {
        qDebug() << "I'm running in the dedicated thread.";
        return QStringLiteral("my data");
      });
      
    p.then(
      [](QString&& s) 
      {
        qDebug() << "Worker result is" << s;
        return nullptr;
      },
      [](const std::exception& e) 
      {
        qDebug() << "Worker threw an exception";
        return nullptr;
      });