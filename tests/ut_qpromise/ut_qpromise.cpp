/** \file ut_qpromise.cpp
  * \brief Unit tests for the QPromise
  * \author Devyatnikov A.
  * \date 29.05.2016
  * Project: QPromise
  */

#include "QPromise.h"

#include <QtTest>

#include <thread>
#include <unordered_map>

class ut_qpromise: public QObject
{
  Q_OBJECT
private slots:
  void preresolved_thenSync()
  {
    QPromise p;
    bool isNotified = false;
    p.resolve(123);

    p.thenSync([&isNotified](int v)
    {
      Q_ASSERT(!isNotified);
      Q_ASSERT(v = 123);
      isNotified = true;
      return nullptr;
    });

    QVERIFY(isNotified);
  }

  void prerejected_thenSync()
  {
    QPromise p;
    bool isNotified = false;
    p.reject(123);

    p.thenSync(
      []()
      {
        Q_ASSERT(false);
        return nullptr;
      },
      [&isNotified](const std::exception_ptr& e)
      {
        Q_ASSERT(!isNotified);
        try { std::rethrow_exception(e); }
        catch (int v)
        {
          Q_ASSERT(v == 123);
          isNotified = true;
        }
        return nullptr;
      });

    QVERIFY(isNotified);
  }

  void postresolved_thenSync()
  {
    QPromise p;
    bool isNotified = false;

    p.thenSync([&isNotified](int v)
    {
      Q_ASSERT(!isNotified);
      Q_ASSERT(v = 123);
      isNotified = true;
      return nullptr;
    });

    QVERIFY(!isNotified);
    p.resolve(123);
    QVERIFY(isNotified);
  }

  void postejected_thenSync()
  {
    QPromise p;
    bool isNotified = false;

    p.thenSync(
      []()
      {
        Q_ASSERT(false);
        return nullptr;
      },
      [&isNotified](const std::exception_ptr& e)
      {
        Q_ASSERT(!isNotified);
        try { std::rethrow_exception(e); }
        catch (int v)
        {
          Q_ASSERT(v == 123);
          isNotified = true;
        }
        return nullptr;
      });

    QVERIFY(!isNotified);
    p.reject(123);
    QVERIFY(isNotified);
  }

  void preresolved_then()
  {
    QPromise p;
    bool isNotified = false;
    p.resolve(123);

    p.then([&isNotified](int v)
    {
      Q_ASSERT(!isNotified);
      Q_ASSERT(v = 123);
      isNotified = true;
      return nullptr;
    });

    QVERIFY(!isNotified);
    QTRY_VERIFY_WITH_TIMEOUT(isNotified, 100);
  }

  void prerejected_then()
  {
    QPromise p;
    bool isNotified = false;
    p.reject(123);

    p.then(
      []()
      {
        Q_ASSERT(false);
        return nullptr;
      },
      [&isNotified](const std::exception_ptr& e)
      {
        Q_ASSERT(!isNotified);
        try { std::rethrow_exception(e); }
        catch (int v)
        {
          Q_ASSERT(v == 123);
          isNotified = true;
        }
        return nullptr;
      });

    QVERIFY(!isNotified);
    QTRY_VERIFY_WITH_TIMEOUT(isNotified, 100);
  }

  void postresolved_then()
  {
    QPromise p;
    bool isNotified = false;

    p.then([&isNotified](int v)
    {
      Q_ASSERT(!isNotified);
      Q_ASSERT(v = 123);
      isNotified = true;
      return nullptr;
    });

    QVERIFY(!isNotified);
    p.resolve(123);
    QVERIFY(!isNotified);
    QTRY_VERIFY_WITH_TIMEOUT(isNotified, 100);
  }

  void postrejected_then()
  {
    QPromise p;
    bool isNotified = false;


    p.then(
      []()
      {
        Q_ASSERT(false);
        return nullptr;
      },
      [&isNotified](const std::exception_ptr& e)
      {
        Q_ASSERT(!isNotified);
        try { std::rethrow_exception(e); }
        catch (int v)
        {
          Q_ASSERT(v == 123);
          isNotified = true;
        }
        return nullptr;
      });

    QVERIFY(!isNotified);
    p.reject(123);
    QVERIFY(!isNotified);
    QTRY_VERIFY_WITH_TIMEOUT(isNotified, 100);
  }

  void preresolved_thenSync_thenSync()
  {
    QPromise p;
    p.resolve(123);
    int notifications = 0;

    p.thenSync([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v = 123);
      notifications = 1;
      return 234;
    })
    .thenSync([&](int v)
    {
      Q_ASSERT(notifications == 1);
      Q_ASSERT(v = 234);
      notifications = 2;
      return nullptr;
    });

    QVERIFY(notifications == 2);
  }

  void preresolved_thenSync_throw_thenSync()
  {
    QPromise p;
    p.resolve(123);
    int notifications = 0;

    p.thenSync(
        [&](int v)
        {
          Q_ASSERT(notifications == 0);
          Q_ASSERT(v = 123);
          notifications = 1;
          throw 234;
          return 0;
        })
    .thenSync(
        [&](int)
        {
          Q_ASSERT(false);
          return nullptr;
        },
        [&](const std::exception_ptr& e)
        {
          Q_ASSERT(notifications == 1);
          try { std::rethrow_exception(e); }
          catch (int v)
          {
            Q_ASSERT(v = 234);
            notifications = 2;
          }
          return nullptr;
        });

    QVERIFY(notifications == 2);
  }

  void postresolved_thenSync_thenSync()
  {
    QPromise p;
    int notifications = 0;

    p.thenSync([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v = 123);
      notifications = 1;
      return 234;
    })
    .thenSync([&](int v)
    {
      Q_ASSERT(notifications == 1);
      Q_ASSERT(v = 234);
      notifications = 2;
      return nullptr;
    });

    QVERIFY(notifications == 0);
    p.resolve(123);
    QVERIFY(notifications == 2);
  }

  void preresolved_then_then()
  {
    QPromise p;
    p.resolve(123);
    int notifications = 0;

    p.then([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v = 123);
      notifications = 1;
      return 234;
    })
    .then([&](int v)
    {
      Q_ASSERT(notifications == 1);
      Q_ASSERT(v = 234);
      notifications = 2;
      return nullptr;
    });

    QVERIFY(notifications == 0);
    QTRY_VERIFY_WITH_TIMEOUT(notifications == 2, 100);
  }

  void postresolved_then_then()
  {
    QPromise p;
    int notifications = 0;

    p.then([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v = 123);
      notifications = 1;
      return 234;
    })
    .then([&](int v)
    {
      Q_ASSERT(notifications == 1);
      Q_ASSERT(v = 234);
      notifications = 2;
      return nullptr;
    });

    QVERIFY(notifications == 0);
    p.resolve(123);
    QVERIFY(notifications == 0);
    QTRY_VERIFY_WITH_TIMEOUT(notifications == 2, 100);
  }

  void preresolved_then_thenSync()
  {
    QPromise p;
    p.resolve(123);
    int notifications = 0;

    p.then([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v = 123);
      notifications = 1;
      return 234;
    })
    .thenSync([&](int v)
    {
      Q_ASSERT(notifications == 1);
      Q_ASSERT(v = 234);
      notifications = 2;
      return nullptr;
    });

    QVERIFY(notifications == 0);
    QTRY_VERIFY_WITH_TIMEOUT(notifications == 2, 100);
  }

  void preresolved_thenSync_then()
  {
    QPromise p;
    p.resolve(123);
    int notifications = 0;

    p.thenSync([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v = 123);
      notifications = 1;
      return 234;
    })
    .then([&](int v)
    {
      Q_ASSERT(notifications == 1);
      Q_ASSERT(v = 234);
      notifications = 2;
      return nullptr;
    });

    QVERIFY(notifications == 1);
    QTRY_VERIFY_WITH_TIMEOUT(notifications == 2, 100);
  }

  void preresolved_thenSync_ret_promise_thenSync()
  {
    QPromise p;
    QPromise p2;
    p.resolve(123);
    int notifications = 0;

    p.thenSync([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v = 123);
      notifications = 1;
      return p2;
    })
    .thenSync([&](int v)
    {
      Q_ASSERT(notifications == 1);
      Q_ASSERT(v = 234);
      notifications = 2;
      return nullptr;
    });

    QVERIFY(notifications == 1);
    p2.resolve(234);
    QVERIFY(notifications == 2);
  }

  void postresolved_thenSync_ret_promise_thenSync()
  {
    QPromise p;
    QPromise p2;

    int notifications = 0;

    p.thenSync([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v = 123);
      notifications = 1;
      return p2;
    })
    .thenSync([&](int v)
    {
      Q_ASSERT(notifications == 1);
      Q_ASSERT(v = 234);
      notifications = 2;
      return nullptr;
    });

    QVERIFY(notifications == 0);
    p.resolve(123);
    QVERIFY(notifications == 1);
    p2.resolve(234);
    QVERIFY(notifications == 2);
  }

  void preresolved_then_ret_promise_then()
  {
    QPromise p;
    QPromise p2;
    p.resolve(123);
    int notifications = 0;

    p.then([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v = 123);
      notifications = 1;
      return p2;
    })
    .then([&](int v)
    {
      Q_ASSERT(notifications == 1);
      Q_ASSERT(v = 234);
      notifications = 2;
      return nullptr;
    });

    QVERIFY(notifications == 0);
    QTRY_VERIFY_WITH_TIMEOUT(notifications == 1, 100);
    p2.resolve(234);
    QVERIFY(notifications == 1);
    QTRY_VERIFY_WITH_TIMEOUT(notifications == 2, 100);
  }

  void postresolved_then_ret_promise_then()
  {
    QPromise p;
    QPromise p2;
    int notifications = 0;

    p.then([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v = 123);
      notifications = 1;
      return p2;
    })
    .then([&](int v)
    {
      Q_ASSERT(notifications == 1);
      Q_ASSERT(v = 234);
      notifications = 2;
      return nullptr;
    });

    QVERIFY(notifications == 0);
    p.resolve(123);
    QTRY_VERIFY_WITH_TIMEOUT(notifications == 1, 100);
    p2.resolve(234);
    QVERIFY(notifications == 1);
    QTRY_VERIFY_WITH_TIMEOUT(notifications == 2, 100);
  }

  void resolve_delay_no_fn()
  {
    QPromise p = QPromise::delay(30);
    int notifications = 0;

    p.thenSync([&]()
    {
      Q_ASSERT(notifications == 0);
      notifications = 1;
      return nullptr;
    });

    QVERIFY(notifications == 0);
    QTRY_VERIFY_WITH_TIMEOUT(notifications == 0, 10);
    QTRY_VERIFY_WITH_TIMEOUT(notifications == 1, 100);
  }

  void resolve_delay_with_fn()
  {
    QPromise p = QPromise::delay(30, []() { return 123; });
    int notifications = 0;

    p.thenSync([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v == 123);
      notifications = 1;
      return nullptr;
    });

    QVERIFY(notifications == 0);
    QTRY_VERIFY_WITH_TIMEOUT(notifications == 0, 10);
    QTRY_VERIFY_WITH_TIMEOUT(notifications == 1, 100);
  }

  void resolve_deferToThreadPool()
  {
    std::thread::id callableThreadId;
    std::thread::id thenThreadId;

    int notifications = 0;
    QMutex mutex;
    mutex.lock();

    QPromise::deferToThreadPool(
    [&]()
    {
      mutex.lock();
      callableThreadId = std::this_thread::get_id();
      mutex.unlock();
      return 123;
    })
    .thenSync([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v == 123);
      notifications = 1;
      thenThreadId = std::this_thread::get_id();
      return nullptr;
    });
    QVERIFY(notifications == 0);
    QVERIFY(callableThreadId == std::thread::id());
    mutex.unlock();

    QTRY_VERIFY_WITH_TIMEOUT(notifications == 1, 100);
    QVERIFY(callableThreadId != std::this_thread::get_id());
    QVERIFY(thenThreadId == std::this_thread::get_id());
  }

  void resolve_deferToThread()
  {
    std::thread::id callableThreadId;
    std::thread::id thenThreadId;

    int notifications = 0;
    QMutex mutex;
    mutex.lock();

    QPromise::deferToThread(
    [&]()
    {
      mutex.lock();
      callableThreadId = std::this_thread::get_id();
      mutex.unlock();
      return 123;
    })
    .thenSync([&](int v)
    {
      Q_ASSERT(notifications == 0);
      Q_ASSERT(v == 123);
      notifications = 1;
      thenThreadId = std::this_thread::get_id();
      return nullptr;
    });
    QVERIFY(notifications == 0);
    QVERIFY(callableThreadId == std::thread::id());
    mutex.unlock();

    QTRY_VERIFY_WITH_TIMEOUT(notifications == 1, 100);
    QVERIFY(callableThreadId != std::this_thread::get_id());
    QVERIFY(thenThreadId == std::this_thread::get_id());
  }

  void multiple_then_resolved_last()
  {
    bool n1 = false;
    bool n2 = false;
    auto p = QPromise();

    p.thenSync([&](int v)
    {
      Q_ASSERT(!n1);
      Q_ASSERT(v == 123);
      n1 = true;
      return nullptr;
    });

    p.thenSync([&](int v)
    {
      Q_ASSERT(!n2);
      Q_ASSERT(v == 123);
      n2 = true;
      return nullptr;
    });

    QVERIFY(!n1 && !n2);
    p.resolve(123);
    QVERIFY(n1 && n2);
  }

  void multiple_then_resolved_in_the_middle()
  {
    bool n1 = false;
    bool n2 = false;
    auto p = QPromise();

    p.thenSync([&](int v)
    {
      Q_ASSERT(!n1);
      Q_ASSERT(v == 123);
      n1 = true;
      return nullptr;
    });

    QVERIFY(!n1);
    p.resolve(123);
    QVERIFY(n1 && !n2);

    p.thenSync([&](int v)
    {
      Q_ASSERT(!n2);
      Q_ASSERT(v == 123);
      n2 = true;
      return nullptr;
    });

    QVERIFY(n1 && n2);
  }

  void preresolved_all_0promises()
  {
    bool isNotified = false;
    auto p = QPromise::all({});
    p.thenSync(
        [&](std::vector<int>&& v)
        {
          Q_ASSERT(!isNotified);
          isNotified = true;
          Q_ASSERT(v.size() == 0);
          return nullptr;
        });

    QVERIFY(isNotified);
  }

  void preresolved_all_1promises()
  {
    QPromise p1 = QPromise().resolve(123);

    bool isNotified = false;
    auto p = QPromise::all({p1});
    p.thenSync(
        [&](std::vector<int>&& v)
        {
          Q_ASSERT(!isNotified);
          isNotified = true;
          Q_ASSERT(v.size() == 1);
          Q_ASSERT(v[0] == 123);

          return nullptr;
        });

    QVERIFY(isNotified);
  }

  void preresolved_all_2promises()
  {
    QPromise p1 = QPromise().resolve(123);
    QPromise p2 = QPromise().resolve(234);

    bool isNotified = false;
    auto p = QPromise::all({p1, p2});
    p.thenSync(
        [&](std::vector<int>&& v)
        {
          Q_ASSERT(!isNotified);
          isNotified = true;
          Q_ASSERT(v.size() == 2);
          Q_ASSERT(v[0] == 123);
          Q_ASSERT(v[1] == 234);

          return nullptr;
        });

    QVERIFY(isNotified);
  }

  void postresolved_all_2promises()
  {
    QPromise p1 = QPromise();
    QPromise p2 = QPromise();

    bool isNotified = false;
    auto p = QPromise::all({p1, p2});
    p.thenSync(
        [&](std::vector<int>&& v)
        {
          Q_ASSERT(!isNotified);
          isNotified = true;
          Q_ASSERT(v.size() == 2);
          Q_ASSERT(v[0] == 123);
          Q_ASSERT(v[1] == 234);

          return nullptr;
        });

    QVERIFY(!isNotified);
    p1.resolve(123);
    QVERIFY(!isNotified);
    p2.resolve(234);
    QVERIFY(isNotified);
  }

  void preresolved_any_0promises()
  {
    bool isNotified = false;
    auto p = QPromise::any({});
    p.thenSync(
        []()
        {
          Q_ASSERT(false); // could be never reached.
          return nullptr;
        },
        [&](const std::exception_ptr& e)
        {
          Q_ASSERT(!isNotified);
          try { std::rethrow_exception(e); } catch (std::exception& v)
          {
            Q_ASSERT(typeid(v) == typeid(std::range_error));
          }
          isNotified = true;
          return nullptr;
        });

    QVERIFY(isNotified);
  }

  void preresolved_any_1promises()
  {
    QPromise p1 = QPromise().resolve(123);

    bool isNotified = false;
    auto p = QPromise::any({p1});
    p.thenSync(
        [&](int v)
        {
          Q_ASSERT(!isNotified);
          isNotified = true;
          Q_ASSERT(v == 123);
          return nullptr;
        });

    QVERIFY(isNotified);
  }

  void preresolved_any_2promises()
  {
    QPromise p1 = QPromise().resolve(123);
    QPromise p2 = QPromise().resolve(234);

    bool isNotified = false;
    auto p = QPromise::any({p1, p2});
    p.thenSync(
        [&](int v)
        {
          Q_ASSERT(!isNotified);
          isNotified = true;
          Q_ASSERT(v == 123 || v == 234); // order is NOT specified!!!
          return nullptr;
        });

    QVERIFY(isNotified);
  }

  void postresolved_any_2promises_1stpromise()
  {
    QPromise p1;
    QPromise p2;

    bool isNotified = false;
    auto p = QPromise::any({p1, p2});
    p.thenSync(
        [&](int v)
        {
          Q_ASSERT(!isNotified);
          isNotified = true;
          Q_ASSERT(v == 123);
          return nullptr;
        });

    QVERIFY(!isNotified);
    p1.resolve(123);
    QVERIFY(isNotified);
  }

  void postresolved_any_2promises_2ndpromise()
  {
    QPromise p1;
    QPromise p2;

    bool isNotified = false;
    auto p = QPromise::any({p1, p2});
    p.thenSync(
        [&](int v)
        {
          Q_ASSERT(!isNotified);
          isNotified = true;
          Q_ASSERT(v == 234);
          return nullptr;
        });

    QVERIFY(!isNotified);
    p2.resolve(234);
    QVERIFY(isNotified);
  }

  void is_operators_works()
  {
    QPromise a1;
    QPromise a2 = a1;


    QVERIFY(a1 == a2);
    QVERIFY(!(a1 < a2));

    std::unordered_map<QPromise, QPromise> map {{a1, a2}};
    QVERIFY(map[a1] == a2);

    QPromise a3;
    a3.swap(a2);
    std::swap(a3, a2);
  }
};

QTEST_MAIN(ut_qpromise)

#include "ut_qpromise.moc"
