/** \file QPromise.h
  * \brief Declaration of the Promises/A+ - like QPromise class
  * \author Devyatnikov A.
  * \date 21.03.2016
  * Project: QPromise
  *
  * This is an adaptation from https://github.com/rhashimoto/poolqueue
  */

#ifndef QPROMISE_H
#define QPROMISE_H

#include "QPromise_impl.h"

#include <QLoggingCategory>

#include <atomic>
#include <functional>
#include <initializer_list>
#include <memory>

class QThreadPool;

#ifndef QPROMISE_EXPORT
# if defined(QPROMISE_DLL_BUILD)
#  define QPROMISE_EXPORT Q_DECL_EXPORT
# elif defined(QPROMISE_DLL)
#  define QPROMISE_EXPORT Q_DECL_IMPORT
# else
#  define QPROMISE_EXPORT
# endif
#endif


/** Promises/A+ style asynchronous operations.
*
* Promise is inspired by the Promises/A+ specification
* (https://promisesaplus.com/):
*
*   A promise represents the eventual result of an asynchronous
*   operation. The primary way of interacting with a promise is
*   through its then method, which registers callbacks to receive
*   either a promise’s eventual value or the reason why the
*   promise cannot be fulfilled.
*
* A Promise instance references shared state. Copying an instance
* provides another reference to the same state. The lifetime of
* the state is independent of the instance; i.e. the state lives
* as long as necessary to propagate results.
*/
class QPROMISE_EXPORT QPromise {
public:
  typedef detail::Any Value;
  typedef detail::bad_cast bad_cast;

  /** Construct a non-dependent Promise.
   *
   * This creates a new instance that is not attached to any other
   * instance.
   */
  QPromise();

  /** Copy constructor.
   *
   * Copying a Promise instance results in a second instance that
   * references the same state as the first, via a shared_ptr.
   */
  QPromise(const QPromise&) = default;

  /** Copy assignment.
   *
   * Copying a Promise instance results in a second instance that
   * references the same state as the first, via a shared_ptr.
   */
  QPromise& operator=(const QPromise&) = default;

  /** Move constructor.
   */
  QPromise(QPromise&&) Q_DECL_NOTHROW;

  /** Move assignment.
   */
  QPromise& operator= (QPromise&& other) Q_DECL_NOTHROW
  {
    pimpl.swap(other.pimpl);
    return *this;
  }

  /** Construct a non-dependent QPromise with callbacks.
   * \param onFulfil
   *            Function/functor to be called if the QPromise is fulfilled.
   *            onFulfil may take a single argument by value, const
   *            reference, or rvalue reference. In the case of an
   *            rvalue reference argument, the QPromise will be closed
   *            to additional callbacks. onFulfil must return a value.
   * \param onReject
   *            Optional function/functor to be called if the QPromise is
   *            rejected. onReject may take a single argument of
   *            const std::exception_ptr& and must return a value.
   *
   * \param delayMs
   *            Optional delay before calling the callback arguments. If this
   *            parameter is set to the value grater or equal to 0, the promise
   *            will work in asynchronious mode. If it is set to -1, the promise
   *            will be synchronious.
   *            - In synchronious mode, the callbacks will be called immediately
   *            from #resolve
   *            - In asynchronious mode, the callbacks will be called later using
   *            the event loop from the thread this constructor called from.
   *            By default, the constructor creates an object in asynchronious
   *            mode. This state is not automatically inherted in the promises
   *            tree. Each QPromise in the tree may operate in the different mode.
   *
   * This creates a new instance that is not attached to any other
   * instance.  When the instance is resolved, the appropriate
   * callback argument will be called.
   */
  template<typename Fulfil, typename Reject = detail::NullReject,
           typename = typename std::enable_if<!std::is_same<typename std::decay<Fulfil>::type, QPromise>::value>::type>
  QPromise(Fulfil&& onFulfil, Reject&& onReject = Reject(), int delayMs = 0)
     : QPromise(
        !std::is_same<typename std::decay<Fulfil>::type, detail::NullFulfil>::value ?
        detail::makeCallbackWrapper(std::forward<Fulfil>(onFulfil)) :
        static_cast<detail::CallbackWrapper *>(nullptr),
        !std::is_same<typename std::decay<Reject>::type, detail::NullReject>::value ?
        detail::makeCallbackWrapper(std::forward<Reject>(onReject)) :
        static_cast<detail::CallbackWrapper *>(nullptr), delayMs)
  {
    typedef typename detail::CallableTraits<Fulfil>::ArgumentType FulfilArgument;
    static_assert(!std::is_same<typename std::decay<FulfilArgument>::type, QPromise>::value,
                 "onFulfil callback cannot take a QPromise argument.");
    static_assert(!std::is_same<typename std::decay<FulfilArgument>::type, std::exception_ptr>::value,
                 "onFulfil callback cannot take a std::exception_ptr argument.");
    typedef typename detail::CallableTraits<Reject>::ArgumentType RejectArgument;
    static_assert(std::is_same<typename std::decay<RejectArgument>::type, std::exception_ptr>::value ||
                 std::is_same<typename std::decay<RejectArgument>::type, void>::value,
                 "onReject callback must take a void or std::exception_ptr argument.");

    Q_CONSTEXPR bool isFulfilNull = std::is_same<typename std::decay<Fulfil>::type, detail::NullFulfil>::value;
    Q_CONSTEXPR bool isRejectNull = std::is_same<typename std::decay<Reject>::type, detail::NullReject>::value;
    typedef typename detail::CallableTraits<Fulfil>::ResultType FulfilResult;
    typedef typename detail::CallableTraits<Reject>::ResultType RejectResult;
    static_assert(isFulfilNull || isRejectNull ||
                 std::is_same<FulfilResult, RejectResult>::value,
                 "onFulfil and onReject return types must match");
    static_assert(!std::is_same<typename std::decay<FulfilResult>::type, void>::value,
                 "onFulfil callback must return a value.");
    static_assert(!std::is_same<typename std::decay<RejectResult>::type, void>::value,
                 "RejectResult callback must return a value.");
  }

  ~QPromise() Q_DECL_NOTHROW;

  /** Resolve a non-dependent QPromise with a value.
   * \param value Copyable or Movable value.
   *
   * \return *this to allow return QPromise().resolve(value);
   */
  template<typename T>
  const QPromise& resolve(T&& value) const
  {
    resolve(Value(std::forward<T>(value)));
    return *this;
  }

  /** Resolve a non-dependent QPromise with an empty value.
   *
   * \return *this to allow return QPromise().resolve();
   **/
  const QPromise& resolve() const
  {
     resolve(Value());
     return *this;
  }

  /** Resolve a non-dependent QPromise with a failure.
   * \param value
   *          Any value type, that will be transformed to std::exception_ptr
   *          and resolved with #resolve. If the type is already std::exception_ptr
   *          this function simply calls #resolve with the value.
   *
   * \return *this to allow return QPromise().reject(value);
   */
  template <typename T>
  const QPromise& reject(T&& value) const
  {
    Q_CONSTEXPR bool isExceptionPtr = std::is_same<typename std::decay<T>::type, std::exception_ptr>::value;
    if (isExceptionPtr)
    {
      return resolve(value);
    }
    else
    {
      try
      {
        throw value;
      }
      catch (...)
      {
        return resolve(std::current_exception());
      }
    }
  }

  /** Attach fulfil/reject callbacks.
   * \param onFulfil
   *            Function/functor to be called if the QPromise is fulfilled.
   *            onFulfil may take a single argument by value, const
   *            reference, or rvalue reference. In the case of an
   *            rvalue reference argument, the QPromise will be closed
   *            to additional callbacks. onFulfil must return a value.
   * \param onReject
   *            Optional function/functor to be called if the QPromise is
   *            rejected. onReject may take a single argument of
   *            const std::exception_ptr& and must return a value.
   *
   * This method produces a dependent QPromise that receives the value
   * or error from the upstream QPromise and passes it through the
   * matching callback. At most one of the callbacks will be invoked,
   * never both.
   *
   * This callbacks will be called asynchroniously using QEventLoop in the
   * thread from which this call has been made.
   *
   * If the executed callback returns a value that is not a
   * QPromise, the QPromise returned by then is fulfilled with that
   * value. If the the executed callback throws an exception, the
   * QPromise returned by then() is rejected with that exception.
   *
   * If the executed callback returns a QPromise q, the QPromise p
   * returned by then() will receive q's value or error (which may
   * not happen immediately).
   *
   * \return Dependent QPromise to receive the eventual result.
   */
  template<typename Fulfil, typename Reject = detail::NullReject >
  QPromise then(Fulfil&& onFulfil, Reject&& onReject = Reject()) const
  {
    typedef typename detail::CallableTraits<Fulfil>::ResultType FulfilResult;
    typedef typename detail::CallableTraits<Reject>::ResultType RejectResult;
    static_assert(!std::is_same<typename std::decay<FulfilResult>::type, void>::value,
                 "onFulfil callback must return a value.");
    static_assert(!std::is_same<typename std::decay<RejectResult>::type, void>::value,
                 "onReject callback must return a value.");
    if (closed())
      throw std::logic_error("QPromise is closed");

    QPromise next(std::forward<Fulfil>(onFulfil), std::forward<Reject>(onReject));
    attach(next);
    return next;
  }

  /** Attach fulfil/reject callbacks.
   * \param onFulfil
   *            Function/functor to be called if the QPromise is fulfilled.
   *            onFulfil may take a single argument by value, const
   *            reference, or rvalue reference. In the case of an
   *            rvalue reference argument, the QPromise will be closed
   *            to additional callbacks. onFulfil must return a value.
   * \param onReject
   *            Optional function/functor to be called if the QPromise is
   *            rejected. onReject may take a single argument of
   *            const std::exception_ptr& and must return a value.
   *
   * This method produces a dependent QPromise that receives the value
   * or error from the upstream QPromise and passes it through the
   * matching callback. At most one of the callbacks will be invoked,
   * never both.
   *
   * This callbacks will be called synchroniously as soon, as #resolve called.
   *
   * If the executed callback returns a value that is not a
   * QPromise, the QPromise returned by then is fulfilled with that
   * value. If the the executed callback throws an exception, the
   * QPromise returned by then() is rejected with that exception.
   *
   * If the executed callback returns a QPromise q, the QPromise p
   * returned by then() will receive q's value or error (which may
   * not happen immediately).
   *
   * \return Dependent QPromise to receive the eventual result.
   */
  template<typename Fulfil, typename Reject = detail::NullReject >
  QPromise thenSync(Fulfil&& onFulfil, Reject&& onReject = Reject()) const
  {
    typedef typename detail::CallableTraits<Fulfil>::ResultType FulfilResult;
    typedef typename detail::CallableTraits<Reject>::ResultType RejectResult;
    static_assert(!std::is_same<typename std::decay<FulfilResult>::type, void>::value,
                 "onFulfil callback must return a value.");
    static_assert(!std::is_same<typename std::decay<RejectResult>::type, void>::value,
                 "onReject callback must return a value.");
    if (closed())
      throw std::logic_error("QPromise is closed");

    QPromise next(std::forward<Fulfil>(onFulfil), std::forward<Reject>(onReject), -1);
    attach(next);
    return next;
  }

  /** Attach reject callback only.
   * \param onReject
   *            Function/functor to be called if the QPromise is
   *            rejected. onReject may take a single argument of
   *            const std::exception_ptr& and must return a value.
   *
   * This method is the same as #then except that it only
   * attaches a reject callback.
   *
   * \return Dependent QPromise to receive the eventual result.
   */
  template<typename Reject>
  QPromise except(Reject&& onReject) const
  {
    typedef typename detail::CallableTraits<Reject>::ResultType RejectResult;
    static_assert(!std::is_same<typename std::decay<RejectResult>::type, void>::value,
                 "onFulfil callback must return a value.");
    return then(detail::NullFulfil(), std::forward<Reject>(onReject));
  }

  /** Attach reject callback only.
   * \param onReject
   *            Function/functor to be called if the QPromise is
   *            rejected. onReject may take a single argument of
   *            const std::exception_ptr& and must return a value.
   *
   * This method is the same as #thenSync except that it only
   * attaches a reject callback.
   *
   * \return Dependent QPromise to receive the eventual result.
   */
  template<typename Reject>
  QPromise exceptSync(Reject&& onReject) const
  {
    typedef typename detail::CallableTraits<Reject>::ResultType RejectResult;
    static_assert(!std::is_same<typename std::decay<RejectResult>::type, void>::value,
                 "onFulfil callback must return a value.");
    return thenSync(detail::NullFulfil(), std::forward<Reject>(onReject));
  }

  /** Disallow future then/except calls.
   *
   * This method explicitly closes a QPromise to disallow calling
   * then() or except(). A closed QPromise may resolve slightly
   * faster than an unclosed QPromise.
   *
   * \return *this
   */
  QPromise& close();

  /** Disallow future then/except calls.
   *
   * This method explicitly closes a QPromise to disallow calling
   * then() or except(). A closed QPromise may resolve slightly
   * faster than an unclosed QPromise.
   *
   * \return *this
   */
  const QPromise& close() const;

  /** Get the resolved state.
   *
   * A QPromise is resolved when it has been either fulfilled or
   * rejected.
   *
   * \return true if resolved.
   */
  bool resolved() const;

  /** Get the closed state.
   *
   * A QPromise is closed when either (1) its close() method has
   * been called or (2) an onFulfil callback that takes an rvalue
   * reference argument has been attached with then().  No
   * additional callbacks can be added to a closed QPromise (i.e.
   * calls to then() or except() will throw an exception).
   *
   * \return true if closed.
   */
  bool closed() const;

  /** Promise execution after some timeout in the current thread QEventLoop
   *
   * \param delay
   *          The delay in milliseconds, after which the promise will
   *          resolve itself.
   * \param callable
   *          The optional function, that will be called after some delay. The
   *          return value of this function will be used to resolve the QPromise.
   *          It is allowed for callable to return an other QPromise.
   *          This callable must not accept any arguments.
   *
   * \return Dependent QPromise that fulfils after specified delay.
   */
  template<typename Callable = detail::NullFulfil>
  static QPromise delay(int delayMs = 0, Callable&& callable = Callable())
  {
    typedef typename detail::CallableTraits<Callable>::ResultType CallableResult;
    typedef typename detail::CallableTraits<Callable>::ArgumentType CallableArgument;
    static_assert(!std::is_same<typename std::decay<CallableResult>::type, void>::value,
                 "callable must return a value.");
    static_assert(std::is_same<typename std::decay<CallableArgument>::type, void>::value,
                 "callable must not accept an argument(s).");

    QPromise rv = QPromise{std::move(callable), detail::NullReject(), delayMs};
    rv.resolve();
    return std::move(rv);
  }

  /** Promise execution the function in the thread pool
   *
   * \param callable
   *          The function, that will be called in the thread pool. The
   *          return value of this function will be used to resolve the QPromise.
   *          It is allowed for callable to return an other QPromise.
   *          This callable must not accept any arguments. The promise will be
   *          resolved in the thread, that called this function.
   * \param pool
   *          The thread pool this function should be executed in. If not
   *          specified, the default Qt thread pool will be used.
   *
   * \return Dependent QPromise that fulfils after function finished it's
   * execution.
   */
  template<typename Callable>
  static QPromise deferToThreadPool(Callable&& callable, QThreadPool* pool = nullptr)
  {
    typedef typename detail::CallableTraits<Callable>::ResultType CallableResult;
    typedef typename detail::CallableTraits<Callable>::ArgumentType CallableArgument;
    static_assert(!std::is_same<typename std::decay<CallableResult>::type, void>::value,
                 "callable must return a value.");
    static_assert(std::is_same<typename std::decay<CallableArgument>::type, void>::value,
                 "callable must not accept an argument(s).");

    return deferToThreadPool(detail::makeCallbackWrapper(std::move(callable)), pool);
  }

  /** Promise execution the function in the different thread.
   *
   * \param callable
   *          The function, that will be called in the new thread. The
   *          return value of this function will be used to resolve the QPromise.
   *          It is allowed for callable to return an other QPromise.
   *          This callable must not accept any arguments. The promise will be
   *          resolved in the thread, that called this function.
   *
   * \return Dependent QPromise that fulfils after function finished it's
   * execution.
   */
  template<typename Callable>
  static QPromise deferToThread(Callable&& callable)
  {
    typedef typename detail::CallableTraits<Callable>::ResultType CallableResult;
    typedef typename detail::CallableTraits<Callable>::ArgumentType CallableArgument;
    static_assert(!std::is_same<typename std::decay<CallableResult>::type, void>::value,
                 "callable must return a value.");
    static_assert(std::is_same<typename std::decay<CallableArgument>::type, void>::value,
                 "callable must not accept an argument(s).");

    return deferToThread(detail::makeCallbackWrapper(std::move(callable)));
  }

  /** Promise conjunction on iterator range.
   *
   * \param bgn Begin iterator.
   * \param end End iterator.
   *
   * This static function returns a QPromise that fulfils when
   * all of the Promises in the input range fulfil, or rejects
   * when any of the Promises in the input range reject.
   *
   * If the returned QPromise fulfils, the values from the input
   * Promises can be received by an onFulfil callback as type
   * std::vector<T> (if the input Promises all return the same
   * type) or as type std::tuple<T0, T1...> (if the number of
   * input Promises is known at compile time). For other cases the
   * value can be retrieved from the individual input Promises.
   *
   * \return Dependent QPromise that fulfils on all or rejects on
   *         any.
   */
  template<typename Iterator>
  static QPromise all(Iterator bgn, Iterator end)
  {
    QPromise p;
    const size_t n = std::distance(bgn, end);
    if (0 != n)
    {
      struct Context
      {
        std::vector<Value> values;
        std::atomic<size_t> count;
        std::atomic<bool> rejected;

        Context(size_t n)
          : values(n)
          , count(n)
          , rejected(false)
        {
        }
      };
      auto context = std::make_shared<Context>(n);

      size_t index = 0;
      for (auto i = bgn; i != end; ++i, ++index)
      {
        i->thenSync(
            [=](const Value& value)
            {
               context->values[index] = value;
               if (context->count.fetch_sub(1) == 1)
               {
                  p.resolve(std::move(context->values));
               }
               return detail::Null();
            },
            [=](const std::exception_ptr& e)
            {
               if (!context->rejected.exchange(true, std::memory_order_relaxed))
                  p.resolve(e);
               return detail::Null();
            });
      }
    }
    else
    {
      // Range is empty so fulfil immediately.
      p.resolve(std::vector<Value>());
    }

    return p;
  }

  /** Promise conjunction on initializer list.
   *
   * \param promises Input promises initializer list.
   *
   * This static function returns a QPromise that fulfils when
   * all of the Promises in the input list fulfil, or rejects
   * when any of the Promises in the input list reject.
   *
   * If the returned QPromise fulfils, the values from the input
   * Promises can be received by an onFulfil callback as type
   * std::vector<T> (if the input Promises all return the same
   * type) or as type std::tuple<T0, T1...> (if the number of
   * input Promises is known at compile time). For other cases the
   * value can be retrieved from the individual input Promises.
   *
   * \return Dependent QPromise that fulfils on all or rejects on
   *         any.
   */
  static QPromise all(std::initializer_list<QPromise> promises)
  {
     return all(promises.begin(), promises.end());
  }

  /** Fulfil with first QPromise of iterator range to fulfil.
   *
   * \param bgn Begin iterator.
   * \param end End iterator.
   *
   * This static function returns a QPromise that fulfils when
   * at least one of the Promises in the input range fulfils,
   * or rejects when all of the Promises in the input range
   * reject.
   *
   * If the returned QPromise rejects, the std::exception_ptr
   * argument does not contain an exception.
   *
   * \return Dependent QPromise that fulfils on any or rejects
   *         on all.
   */
  template<typename Iterator>
  static QPromise any(Iterator bgn, Iterator end)
  {
     QPromise p;
     const size_t n = std::distance(bgn, end);
     if (0 != n)
     {
        struct Context
        {
           std::atomic<size_t> count;
           std::atomic<bool> fulfilled;

           Context(size_t n)
              : count(n)
              , fulfilled(false) {
           }
        };
        auto context = std::make_shared<Context>(n);

        for (auto i = bgn; i != end; ++i)
        {
           i->thenSync(
              [=](const Value& value)
              {
                 if (!context->fulfilled.exchange(true, std::memory_order_relaxed))
                    p.resolve(value);
                 return detail::Null();
              },
              [=](const std::exception_ptr&)
              {
                 if (context->count.fetch_sub(1, std::memory_order_relaxed) == 1)
                    p.resolve(std::exception_ptr());
                 return detail::Null();
              });
        }
     }
     else
     {
        // Range is empty so reject immediately.
        p.reject(std::range_error("QPromise range is empty"));
     }

     return p;
  }

  /** Fulfil with first QPromise of initializer list to fulfil.
   *
   * \param promises Input promises.
   *
   * This static function returns a QPromise that fulfils when
   * at least one of the Promises in the input list fulfils,
   * or rejects when all of the Promises in the input list
   * reject.
   *
   * If the returned QPromise rejects, the std::exception_ptr
   * argument does not contain an exception.
   *
   * \return Dependent QPromise that fulfils on any or rejects
   *         on all.
   */
  static QPromise any(std::initializer_list<QPromise> promises)
  {
     return any(promises.begin(), promises.end());
  }

  /** Set undelivered exception handler.
   *
   * \param handler Has signature void handler(const std::exception_ptr&).
   *
   * Callback exceptions that are never delivered to an
   * onReject callback are passed to a global handler that can
   * be set with this function. A copy of the previous handler
   * is returned. The default handler calls std::unexpected().
   *
   * Note that the handler is called from the QPromise destructor
   * so the handler should not throw.
   *
   * \return Copy of the previous handler.
   */
  typedef std::function<void(const std::exception_ptr&)> ExceptionHandler;
  static ExceptionHandler setUndeliveredExceptionHandler(const ExceptionHandler& handler);

  /** Set bad cast exception handler.
   *
   * \param handler Has signature void handler(const QPromise::bad_cast&).
   *
   * If the upstream value does not match the argument of an
   * onFulfil callback a QPromise::bad_cast exception is
   * thrown. This mismatch between output and input is usually
   * a programming error, so the default action is to let this
   * exception propagate in the normal manner (i.e. unwind the
   * stack) instead of capturing it to pass to the next
   * QPromise.
   *
   * If a different behavior is desired, the default handler
   * can be replaced. If the replacement handler does not throw
   * then the exception will be captured.
   *
   * \return Copy of the previous handler.
   */
  typedef std::function<void(const QPromise::bad_cast&)> BadCastHandler;
  static BadCastHandler setBadCastExceptionHandler(const BadCastHandler& handler);

  friend bool operator==(const QPromise& a, const QPromise& b) {
     return a.pimpl == b.pimpl;
  }

  friend bool operator<(const QPromise& a, const QPromise& b) {
     return a.pimpl < b.pimpl;
  }

  size_t hash() const {
     return std::hash<Pimpl *>()(pimpl.get());
  }

  void swap(QPromise& other) {
     pimpl.swap(other.pimpl);
  }

private:
  struct Pimpl;
  std::shared_ptr<Pimpl> pimpl;

  QPromise(detail::CallbackWrapper *, detail::CallbackWrapper *, int);

  void resolve(Value&& result) const;
  void attach(const QPromise& next) const;
  static QPromise deferToThreadPool(detail::CallbackWrapper *callable, QThreadPool* pool);
  static QPromise deferToThread(detail::CallbackWrapper *callable);
};

inline void swap(QPromise& a, QPromise& b)
{
  a.swap(b);
}

Q_DECLARE_LOGGING_CATEGORY(QPromiseLogCategory)

namespace std
{
   template<>
   struct hash<QPromise>
   {
      size_t operator()(const QPromise& p) const
      {
         return p.hash();
      }
   };
}

#endif // QPROMISE_H
