TEMPLATE = app

CONFIG += console warn_on depend_includepath testcase
CONFIG -= app_bundle

QT = testlib core

SOURCES += \
    ut_qpromise.cpp

# First usage way: compile in full sources
include($$PWD/../../QPromise.pri))

# Second usage way: link with the library
#INCLUDEPATH += $$PWD/../../include
#DESTDIR = $$PWD/../../output
#LIBS += -L$$PWD/../../output -lQPromise

# If the library has been built dynamically
#DEFINES += QPROMISE_DLL
